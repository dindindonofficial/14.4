#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
	std::string text;
	std::cin >> text;
    std::cout << "\n" << text.length() << "\n" << text.front() << "\n" << text.back();
}